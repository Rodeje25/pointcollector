﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreCounter: MonoBehaviour {

	//setting the score to zero and giving Text instruction
	public static int counter = 0;
	Text instruction;

	void Start() {
		instruction = GetComponent<Text>();
	}

		void Update()
	{
		instruction.text = counter.ToString();
	}


}

//the code for adding the point. this must be added to the script wich must trigger a point
ScoreCounter.counter+=1;