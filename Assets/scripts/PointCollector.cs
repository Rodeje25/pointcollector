using UnityEngine;
using System.Collections;

public class PointCollector : MonoBehaviour {

	public AudioClip end_game;

	public ParticleSystem essence;

	private bool collected;
	



	
	void Start()
	{
		essence.Clear();
		essence.Stop();
	}

	//void OnTriggerEnter1(Collider coll) {
		//if (coll.tag=="Player")

	//}

	void OnTriggerEnter(Collider coll) {
		if (coll.tag=="Player" && !collected)
		{
			StartCoroutine(BallCollected());
			collected = true;
		}
	}

	IEnumerator BallCollected()
	{
		audio.clip = end_game;
		audio.Play();
		renderer.material.color = Color.black;
		ScoreCounter.counter+=1;
		yield return new WaitForSeconds(6.5f);
		essence.Play();
		Destroy(transform.gameObject);
		yield return new WaitForSeconds(5.0f);
		essence.Stop();

	}




}